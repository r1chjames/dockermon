'use strict';

const Docker = require('dockerode');
const fs = require('fs');
const Config = require('merge-config');
const config = new Config();
let docker;

//Default settings
config.file('default_settings.json');


//Read settings file if it exists
const config_dir = process.env.config_dir || "./config";
console.log("Loading settings from " + config_dir);
config.file(config_dir);

//Attempt to connect to the Docker daemon
switch (config.get("docker_connection:type")) {
    case "http":
        docker = new Docker({ host: config.get("docker_connection:host"), port: config.get("docker_connection:port") });
    break;

    case "socket":
        //Check if the socket is okay
        try{
            let stats = fs.statSync(config.get("docker_connection:path"));

            if (!stats.isSocket()) {
                throw new Error('Unable to connect to Docker socket at ' + config.get("docker_connection:path") + ". Is Docker running?");
            }
        } catch (e) {
            console.error('Unable to connect to Docker socket at ' + config.get("docker_connection:path") + ". Is Docker running?");
            if (config.get("debug"))
                console.log(e);
            process.exit(1);
        }
        //Socket is okay, connect to it
        docker = new Docker({ socketPath: config.get("docker_connection:path") });
    break;

    default:
        throw new Error("Docker connection type " + config.get("docker_connection:type") + " is invalid");
    break;
}


function getContainer(name, cb, error) {
    docker.listContainers({ limit:100, filters: { "name": [name] } }, function (err, containers) {
        if (err) {
            if (typeof error == "function")
                return error(500, err);

            return;
        }

        if (containers.length > 0) {
            //What is the ID of this container?
            //We need to only return the ID as it matches exactly
            for(id in containers) {
                //Does this container have names set?
                if (containers[id].Names.length) {
                    //Yes it does, loop over all names to see if we get one
                    for(i in containers[id].Names) {
                        if (containers[id].Names[i] == "/" + name) {
                            //Found it by name!
                            return cb(containers[id]);
                        }
                    }
                }
            }
        }

        docker.listContainers({ filters: { "id": [name] } }, function (err, containers) {
            if (err) {
                if (typeof error == "function")
                    return error(500, err);
    
                return;
            }
    
            if (containers.length < 1) {
                if (typeof error == "function")
                    return error(404, "container not found");
                
                return;
            }
    
            //What is the ID of this container?
            //We need to only return the ID as it matches exactly
            for(id in containers) {
                //Does this container have names set?
                if (containers[id].Names.length) {
                    //Yes it does, check the first name
                    if (containers[id].Id == name) {
                        //Found it by name!
                        return cb(containers[id]);
                    }
                }
            }

            //Could not find that container - sad face
            if (typeof error == "function")
                return error(404, "container not found");
            
            return false;
        });
    });
}

function stopContainer(container_id) {
    dockerService.getContainer(container_id).start(function (err, data) {
        if (err) {
            if (config.get("debug")) {
                console.log("Failed to start container " + container.Id);
                console.log(err);
            }

            res.status(500);
            res.send(err);
            return res;
        }
        if (config.get("debug"))
            console.log("Container started");

        res.status(200);
        res.send({
            state: "running"
        });
        return res;
    });
}