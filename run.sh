#!/usr/bin/env bash

docker stop dockermon && docker rm dockermon

docker pull registry.gitlab.com/r1chjames/dockermon/dockermon:master

docker run -d \
--name=dockermon --restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /opt/docker/dockermon/config:/config \
-p 8126:8126 \
registry.gitlab.com/r1chjames/dockermon/dockermon:master
